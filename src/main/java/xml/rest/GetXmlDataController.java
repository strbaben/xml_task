package xml.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xml.entity.CastObce;
import xml.entity.Obec;
import xml.service.CastObceService;
import xml.service.GetXMLDataService;
import xml.service.ObecService;

import java.util.List;

@RestController
@RequestMapping(value = "/")
public class GetXmlDataController {

    @Autowired
    private ObecService obecService;

    @Autowired
    private CastObceService castObceService;

    @Autowired
    GetXMLDataService getXMLDataService;

    /**
     * method downloads data, extract desired informations and save it
     */
    @GetMapping(value = "/download")
    public void downloadData() {
        getXMLDataService.downloadXML("tmp.zip","https://vdp.cuzk.cz/vymenny_format/soucasna/20200630_OB_573060_UZSZ.xml.zip");
        getXMLDataService.init("tmp.zip");
        List<Obec> obceResult = getXMLDataService.getObce();
        List<CastObce> castObceResult = getXMLDataService.getCastiObce();

        obecService.saveObce(obceResult);
        castObceService.saveCastObci(castObceResult);
    }
}
