package xml.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CastObce {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cast_obce_id")
    private Long castObceId;

    @Column(name = "kod")
    private long kod;

    @Column(name = "kod_obce")
    private long kodObce;

    @Column(name = "nazev")
    private String nazev;

    public CastObce() {
    }

    public CastObce(long kod, long kodObce, String nazev) {
        this.kod = kod;
        this.kodObce = kodObce;
        this.nazev = nazev;
    }

    public Long getCastObceId() {
        return castObceId;
    }

    public void setCastObceId(Long castObceId) {
        this.castObceId = castObceId;
    }

    public long getKod() {
        return kod;
    }

    public void setKod(long kod) {
        this.kod = kod;
    }

    public long getKodObce() {
        return kodObce;
    }

    public void setKodObce(long kodObce) {
        this.kodObce = kodObce;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
}
