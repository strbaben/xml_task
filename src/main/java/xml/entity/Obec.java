package xml.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "obec")
public class Obec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "obec_id")
    private Long obecId;

    @Column(name = "kod")
    private long kod;

    @Column(name = "nazev")
    private String nazev;

    public Obec() {
    }

    public Obec(long kod, String nazev) {
        this.kod = kod;
        this.nazev = nazev;
    }

    public Long getObecId() {
        return obecId;
    }

    public void setObecId(Long obecId) {
        this.obecId = obecId;
    }

    public long getKod() {
        return kod;
    }

    public void setKod(long kod) {
        this.kod = kod;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
}
