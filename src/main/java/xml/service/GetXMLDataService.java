package xml.service;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.entity.CastObce;
import xml.entity.Obec;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Service
public class GetXMLDataService {

    private Document document;

    /**
     * method initialize document from which we get data
     * @param filename is name of the downloaded zipfile
     */
    public void init(String filename) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;

        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            ZipFile zipFile = new ZipFile(filename);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipEntry zipEntry = entries.nextElement();
            InputStream inputStream = zipFile.getInputStream(zipEntry);
            document = documentBuilder.parse(inputStream);
            zipFile.close();
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * method will get info about Obce from downloaded data
     * @return list of entries to save
     */
    public List<Obec> getObce() {
        NodeList obce = document.getElementsByTagName("vf:Obec");
        List<Obec> result = new ArrayList<>();

        for(int i = 0; i < obce.getLength(); i++) {
            Element obec = (Element) obce.item(i);
            NodeList kod = obec.getElementsByTagName("obi:Kod");
            NodeList nazev = obec.getElementsByTagName("obi:Nazev");
            result.add(new Obec(Long.parseLong(kod.item(0).getTextContent()), nazev.item(0).getTextContent()));
        }
        return result;
    }

    /**
     * method will get info about Casti Obce from downloaded data
     * @return list of entries to save
     */
    public List<CastObce> getCastiObce() {
        NodeList castiObce = document.getElementsByTagName("vf:CastObce");
        List<CastObce> result = new ArrayList<>();

        for(int i = 0; i < castiObce.getLength(); i++) {
            Element castObce = (Element) castiObce.item(i);
            NodeList kod = castObce.getElementsByTagName("coi:Kod");
            NodeList kodObce = castObce.getElementsByTagName("obi:Kod");
            NodeList nazev = castObce.getElementsByTagName("coi:Nazev");
            result.add(new CastObce(Long.parseLong(kod.item(0).getTextContent()),
                    Long.parseLong(kodObce.item(0).getTextContent()),
                    nazev.item(0).getTextContent()));
        }
        return result;
    }

    /**
     * method downloads data and saves it
     * @param filename of the saved file
     * @param url from where to downlaod data
     */
    public void downloadXML(final String filename,final String url) {
        BufferedInputStream in = null;
        FileOutputStream fout = null;
        try {
            in = new BufferedInputStream(new URL(url).openStream());
            fout = new FileOutputStream(filename);

            byte[] buffer = new byte[1024];
            int count;
            while((count = in.read(buffer, 0, 1024)) != -1) {
                fout.write(buffer, 0, count);
            }
            fout.close();
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
