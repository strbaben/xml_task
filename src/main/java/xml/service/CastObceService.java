package xml.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xml.entity.CastObce;
import xml.repository.CastObceRepository;

@Service
public class CastObceService {

    @Autowired
    CastObceRepository castObceRepository;

    /**
     * method saves one entry
     * @param castObce to save
     */
    public void saveCastObce(CastObce castObce) {
        if(castObce != null)
            castObceRepository.save(castObce);
    }

    /**
     * method saves list of entries
     * @param castObceList to save
     */
    public void saveCastObci(List<CastObce> castObceList) {
        castObceList.stream().forEach(this::saveCastObce);
    }
}
