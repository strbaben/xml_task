package xml.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xml.entity.Obec;
import xml.repository.ObecRepository;

import java.util.List;

@Service
public class ObecService {

    @Autowired
    ObecRepository obecRepository;

    /**
     * method saves one entry
     * @param obec to save
     */
    public void saveObec(Obec obec) {
        if (obec != null)
            obecRepository.save(obec);
    }
    /**
     * method saves list of entries
     * @param obecList to save
     */
    public void saveObce(List<Obec> obecList) {
        obecList.stream().forEach(this::saveObec);
    }
}
