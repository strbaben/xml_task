package xml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xml.entity.Obec;

@Repository
public interface ObecRepository extends JpaRepository<Obec, Long> {

}
