package xml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xml.entity.CastObce;

@Repository
public interface CastObceRepository extends JpaRepository<CastObce, Long> {

}
